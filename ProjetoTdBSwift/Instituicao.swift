//
//  Instituicao.swift
//  ProjetoTdBSwift
//
//  Created by Mauricio Junior on 17/10/17.
//  Copyright © 2017 Mauricio Junior. All rights reserved.
//

import UIKit

class Instituicao: NSObject {

    var idInstituicao: String? = nil
    var nomeInstituicao: String? = nil
    var bairroInstituicao: String? = nil
    var cep: String? = nil
    var enderecoInstituicao: String? = nil
    
    
    func initInstituicao(id: String, nome: String, bairro:String, endereco:String) -> Instituicao {
        
        self.idInstituicao = id
        self.nomeInstituicao = nome
        self.bairroInstituicao = bairro
        
        self.enderecoInstituicao = endereco
        
        return self
    }
    
    func initIdENome(id: String, nome: String) -> Instituicao {
        self.idInstituicao = id
        self.nomeInstituicao = nome
        
        return self
    }
}
