//
//  InstituicaoTableViewController.swift
//  ProjetoTdBSwift
//
//  Created by Mauricio Junior on 17/10/17.
//  Copyright © 2017 Mauricio Junior. All rights reserved.
//

import UIKit
import Alamofire

class InstituicaoTableViewController: UITableViewController {

    var instituicoesArray: [Instituicao] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        getInstituition()
        self.tableView.reloadData()
    }
    
    func getInstituition(){
        
        let endereco = "https://cs92.salesforce.com/services/data/v40.0/sobjects/Instituicao_de_Ensino__c"
        
        let header = ["Authorization": " \(GlobalCommom.sharedInstance.tokenType! + " " + GlobalCommom.sharedInstance.token!)"]
        //let parametros = "Authorization: Bearer 00D3F0000000NDl!AQsAQCanH7mMb7pK2y.VCbN02iVuZ_M3njG5xHLJ5gNDGbRC9NRnBxOb7GsWwEtUe4IeV.2jzfe2d1rB9CaEBCQPnbzeYHV6"
        print("Endereço e parametro: \(endereco)\(header)")
        
        Alamofire.request(endereco, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            switch response.result {
            case .success:
                if let json = response.result.value{
                    let dict = json as! Dictionary<String,Any>
                    
                    //print(dict["objectDescribe"])
                    //print(dict["recentItems"]![0]["Name"] as! Dictionary<String,Any>)
                    if let recentItens = dict["recentItems"] as? [Dictionary<String,Any>]{
                        
                        //print(recentItens[0]["Name"])
                        
                        for institucoes in recentItens {
                            self.getInstituitionDetail(IdInstituicao: institucoes["Id"] as! String, NomeInstituicao: institucoes["Name"] as! String)
                            //self.getInstituitionDetail(IdInstituicao: recentItens[0]["Id"] as! String)
                        }
                    }
                    //print(dict["recentItems"]![0]["Id"])
                    self.tableView.reloadData()
                }
            case .failure(let error):
                print(error)
            }
        }
        
        self.tableView.reloadData()
    }
    
    func getInstituitionDetail(IdInstituicao: String, NomeInstituicao: String){
        
        let endereco = "https://cs92.salesforce.com/services/data/v40.0/sobjects/Instituicao_de_Ensino__c/\(IdInstituicao)"
        
        let header = ["Authorization": " \(GlobalCommom.sharedInstance.tokenType! + " " + GlobalCommom.sharedInstance.token!)"]
        //let parametros = "Authorization: Bearer 00D3F0000000NDl!AQsAQCanH7mMb7pK2y.VCbN02iVuZ_M3njG5xHLJ5gNDGbRC9NRnBxOb7GsWwEtUe4IeV.2jzfe2d1rB9CaEBCQPnbzeYHV6"
        print("Endereço e parametro getInstituitionDetail: \(endereco)\(header)")
        
        Alamofire.request(endereco, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            switch response.result {
            case .success:
                if let json = response.result.value{
                    let dict = json as! Dictionary<String,Any>
                    
                    let _bairro = dict["Bairro__c"]!
                    let _endereco = dict["Endereco__c"]!
                    let _cidade = dict["Cidade__c"]!
                    //let _cep = dict["CEP__c"]!
                    let _idInstituicao = IdInstituicao
                    let _nomeInstituicao = NomeInstituicao
                    
                    let _instituicao = Instituicao().initInstituicao(id: IdInstituicao, nome: NomeInstituicao, bairro: _bairro as! String, endereco: endereco)
                    self.instituicoesArray.append(_instituicao)
                    self.tableView.reloadData()
                    
                }
            case .failure(let error):
                print(error)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        print("Numero de instituicoes: \(instituicoesArray.count)")
        return instituicoesArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        let _instituicao = self.instituicoesArray[indexPath.row]
        
        
        // Configure the cell...
        cell.textLabel?.text = _instituicao.nomeInstituicao
        
        //var _idInstituicao = instituicoesArray[indexPath.row]["Id"] as? String
        
        var _bairroEEndereco = _instituicao.bairroInstituicao! + _instituicao.enderecoInstituicao!
        
        cell.detailTextLabel?.text = _bairroEEndereco

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
