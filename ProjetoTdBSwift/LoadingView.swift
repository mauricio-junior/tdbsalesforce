//
//  LoadingView.swift
//  ProjetoTdBSwift
//
//  Created by Mauricio Junior on 18/10/17.
//  Copyright © 2017 Mauricio Junior. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class LoadingView: UIView {

    override func draw(_ rect: CGRect) {
        let currentViewControllerView =  UIApplication.shared.keyWindow?.subviews.last
        currentViewControllerView!.isUserInteractionEnabled = false
        
        self.frame = rect
        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        
        let actIndicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: rect.width*0.1, height: rect.height*0.1), type: NVActivityIndicatorType.ballScaleMultiple, color: #colorLiteral(red: 0.5764705882, green: 0.5960784314, blue: 0, alpha: 1), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        actIndicator.center = self.center
        actIndicator.startAnimating()
        
        let screen = UIScreen.main.bounds
        
        let loadingLabel = UILabel()
        loadingLabel.frame.size.width = screen.width
        loadingLabel.frame.size.height = 21
        loadingLabel.text = "Carregando..."
        loadingLabel.font = loadingLabel.font.withSize(18)
        loadingLabel.textAlignment = .center
        loadingLabel.numberOfLines = 0
        loadingLabel.center = self.center
        loadingLabel.center.y += 50
        loadingLabel.textColor = .white
        
        self.addSubview(actIndicator)
        self.addSubview(loadingLabel)
        
        currentViewControllerView!.addSubview(self)
        
    }
    
    override func removeFromSuperview() {
        let currentViewControllerView =  UIApplication.shared.keyWindow?.subviews.last
        currentViewControllerView!.isUserInteractionEnabled = true
        super.removeFromSuperview()
    }

}
