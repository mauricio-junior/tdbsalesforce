//
//  GlobalCommom.swift
//  ProjetoTdBSwift
//
//  Created by Mauricio Junior on 17/10/17.
//  Copyright © 2017 Mauricio Junior. All rights reserved.
//

import UIKit

class GlobalCommom: NSObject {

    var token: String? = nil
    var tokenType: String? = nil
    
    class var sharedInstance : GlobalCommom {
        struct Singleton {
            static let instance = GlobalCommom()
        }
        
        return Singleton.instance
    }
}
