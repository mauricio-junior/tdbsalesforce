//
//  ViewController.swift
//  ProjetoTdBSwift
//
//  Created by Mauricio Junior on 17/10/17.
//  Copyright © 2017 Mauricio Junior. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    @IBOutlet weak var btnEntrar: UIButton!
    @IBOutlet weak var txtUsuario: UITextField!
    @IBOutlet weak var txtSenha: UITextField!
    let loading = LoadingView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Entrar(_ sender: Any) {
        //tela de loading
        self.loading.draw(self.view.frame)
        
        self.login()
        
        //removendo tela de login
        self.loading.removeFromSuperview()
    }
    
    func login() {
        //aplicativo@tdb.org.br.dev082017
        //let _usuario = self.txtUsuario.text
        
        //app1234
        //let _senha = self.txtSenha.text
        let endereco = "https://cs92.salesforce.com/services/oauth2/token"
        
        let parametros = ["username":"\(self.txtUsuario.text!)",
                          "password":"\(self.txtSenha.text!)",
                          "client_id": "3MVG9JamK_x9K2XL064eeyrwJwZKS11ox_FL.44qcGarEcTH5UO4qxz8CrbYCKixwW3O8HGslu.t8tic3wKis",
                          "client_secret" : "8788358403969105484",
                          "grant_type":"password"]
        
        print("Parametros para o token: \(parametros)")
        
        Alamofire
            .request(
                endereco, method: .post,
                parameters:parametros
            )
            .responseJSON { response in

                //print(response)

                switch response.result {
                    case .success:
                        if let json = response.result.value{
                             let dict = json as! Dictionary<String,Any>
                            
                            //print("TOKEN: \(String(describing: dict["access_token"]))")
                            //print(response)
                            GlobalCommom.sharedInstance.token = dict["access_token"]! as? String
                            GlobalCommom.sharedInstance.tokenType = dict["token_type"]! as? String
                            
                            self.performSegue(withIdentifier: "segueLogin", sender: self)
                        }
                    case .failure(let error):
                        print(error)
                }
        }
    }
    
    
}

